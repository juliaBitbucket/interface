<?php
namespace task2;

require_once 'toys.php';
require_once 'device.php';
require_once 'clothes.php';

//  crblrf 10%
$toys = new Toys(2, 3);
$toys->setPrice(890);
$priceToys = $toys->getPrice();
$totalToys = $toys->getTotal();

// скидка 10% если вес больше 10 кг
$device = new Device(11, 'TV', 'Samsung');
$device->setPrice(3160);
$priceDevice = $device->getPrice();
$totalDevice = $device->getTotal();

// продукт без скидки
$clothes = new Clothes(1, 'M');
$clothes->setPrice(1510);
$priceClothes = $clothes->getPrice();
$totalClothes = $clothes->getTotal();


echo 'Price Toys: ' .$priceToys . ' Shipping: ' . ($totalToys - $priceToys) . ' Total: ' . $totalToys . "\n";
echo 'Price Device: ' .$priceDevice . ' Shipping: ' . ($totalDevice - $priceDevice) . ' Total: ' . $totalDevice .  "\n";
echo 'Price Clothes: ' .$priceClothes . ' Shipping: ' . ($totalClothes - $priceClothes) . ' Total: ' . $totalClothes . "\n";
exit;

