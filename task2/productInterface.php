<?php
namespace task2;

interface ProductInterface {
    public function getPrice();
}