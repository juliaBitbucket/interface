<?php
namespace task2;

require_once 'product.php';

class Device extends Product{
    private $type;
    private $brand;
    private $maxWeight = 10;

    public function __construct($weight, $type, $brand)
    {
        parent::__construct($weight);
        $this->setDiscount(10);
        $this->setShipping();
        $this->setBrand($brand);
        $this->setType($type);
        $this->setCategory('device');
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setDiscount($discount)
    {
        if ($this->getWeight() > $this->maxWeight) {
            parent::setDiscount($discount);
        }
    }
}