<?php
namespace task2;

require_once 'product.php';

class Toys extends Product{
    private $color;
    private $age;

    public function __construct($weight, $age)
    {
        parent::__construct($weight);
        $this->setAge($age);
        $this->setDiscount(10);
        $this->setShipping();
        $this->setCategory('toys');

    }

    public function setColor($color)
    {
        $this->color = $color;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setAge($age)
    {
        $this->age = $age;
    }

    public function getAge()
    {
        return $this->age;
    }
}