<?php
namespace task2;

require_once 'productInterface.php';

class Product implements ProductInterface{
    protected $price;
    protected $category;
    protected $weight;
    protected $discount = 0;
    protected $shipping = 250;
    protected $total;

    public function __construct($weight)
    {
        $this->setWeight($weight);
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getPrice()
    {
        $this->setPrice(round($this->price * (100 - $this->discount)/100));
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setShipping()
    {
        if ($this->getDiscount() > 0) {
            $this->shipping = 300;
        }
    }

    public function getShipping()
    {
        return $this->shipping;
    }

    public function getTotal()
    {
        return $this->price + $this->shipping;
    }
}
