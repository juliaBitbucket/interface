<?php
namespace task2;

require_once 'product.php';

class Clothes extends Product{
    private $size;

    public function __construct($weight, $size)
    {
        parent::__construct($weight);
        $this->setShipping();
        $this->setSize($size);
        $this->setCategory('clothes');
    }

    public function setSize($size)
    {
        $this->size = $size;
    }

    public function getSize()
    {
        return $this->size;
    }
}