<?php

class Toys{
    private $year;

    public function __construct()
    {
        $this->year = date('Y-m-d');
    }


    public function getYear()
    {
        return $this->year;
    }
}
/******************* Car *************************/
class Car extends Toys{
    private $color = 'white';
    private $transmission = 'manual';
    private $fuel = 'petrol';


    public function __construct()
    {
        parent::__construct();
    }

    public function getColor()
    {
        return $this->color;
    }

    public function getTransmission()
    {
        return $this->transmission;
    }

    public function getFuel()
    {
        return $this->fuel;
    }

    public function setColor($value)
    {
        $this->color = $value;
    }

    public function setTransmission($value)
    {
        $this->transmission = $value;
    }

    public function setFuel($value)
    {
        $this->fuel = $value;
    }

    public function start() {
        echo 'Start a car.';
    }

    public function muffle() {
        echo 'Muffle car.';
    }
}

$carGreen = new Car();
$carGreen->setColor('green');

$carAuto = new Car();
$carAuto->setTransmission('auto');

/***************** TV ***************************/
class TV extends Toys{
    private $size;
    private $category;


    public function __construct($size, $category)
    {
        parent::__construct();
        $this->setSize($size);
        $this->setCategory($category);
    }

    public function getSize()
    {
        return $this->size;
    }

    public function getCategory()
    {
        return $this->category;
    }

    private function setSize($value)
    {
        $this->size = $value;
    }

    private function setCategory($value)
    {
        $this->category = $value;
    }

    public function turnOff() {
        echo 'TV turnOff.';
    }

    public function turnOn() {
        echo 'TV turnOff.';
    }
}

$tv = new TV(23, 'samsung');

$newTV = new TV(52, 'LG');

/***************** Ball pen ***************************/
class BallPen extends Toys{
    private $made = 'plastic';
    private $size = 20;

    public function __construct($color)
    {
        parent::__construct($color);
    }

    public function getMade()
    {
        return $this->made;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setMade($value)
    {
        $this->made = $value;
    }

    public function setSize($value)
    {
        $this->size = $value;
    }

    public function write() {
        echo 'Start write a ball pen.';
    }
}

$ballPen = new BallPen('red');

$newBallPen = new BallPen('black');
$newBallPen->setMade('wood');

/***************** Duck ***************************/
class Duck extends Toys{
    private $made = 'wood';
    private $size = 'little';


    public function __construct()
    {
        parent::__construct();
    }

    public function getMade()
    {
        return $this->made;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setMade($value)
    {
        $this->made = $value;
    }

    public function setSize($value)
    {
        $this->size = $value;
    }

    public function swims() {
        echo 'Start swims a duck.';
    }
}

$duck = new Duck();

$newDuck = new Duck();
$newDuck->setMade('plastic');

/***************** Product ***************************/
class Product extends Toys{
    private $brand = 'Hahn';
    private $type;
    private $color;

    public function __construct($type, $color, $category)
    {
        parent::__construct($category);
        $this->setType($type);
        $this->setColor($color);
    }

    public function getType()
    {
        return $this->type;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function getColor()
    {
        return $this->color;
    }

    private function setType($value)
    {
        $this->type = $value;
    }

    private function setColor($value)
    {
        $this->color = $value;
    }

    public function getPrice() {
        if ($this->category == 'quartz') {
            return 20;
        }
        return 30;
    }
}

$product = new Product('2cm', 'white', 'quartz');

$newProduct = new Product('3cm', 'crime', 'quartz');



print_r($carAuto);
print_r($carGreen);
print_r($newTV);
print_r($tv);
print_r($ballPen);
print_r($newBallPen);
print_r($duck);
print_r($newDuck);
print_r($product);
print_r($newProduct);